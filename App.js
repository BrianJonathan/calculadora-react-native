//import { StatusBar } from "expo-status-bar";
import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import {
  Provider as PaperProvider,
  TextInput,
  Headline,
  IconButton,
  Surface,
} from "react-native-paper";

//import IconButton from "./IconButton";
//import StackLayout from './layouts/StackLayout';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { resultado: "", valor1: "", valor2: "" };

    this.sumar = this.sumar.bind(this);
    this.setValor1 = this.setValor1.bind(this);
    this.setValor2 = this.setValor2.bind(this);
  }

  render() {
    return (
      <PaperProvider>
        <View style={styles.container}>
          <Headline style={styles.headline}> Calculadora React!</Headline>

          <TextInput
            value={this.state.valor1}
            style={styles.textInput}
            label="Ingrese valor 1"
            onChangeText={this.setValor1}
          ></TextInput>

          <TextInput
            value={this.state.valor2}
            style={styles.textInput}
            label="Ingrese valor 2"
            onChangeText={this.setValor2}
          ></TextInput>

          {/* <StatusBar style="auto" /> */}
          <View style={{ flexDirection: "row" }}>
            <View style={{ width: 20 }} />
            <Surface style={styles.surface}>
              <IconButton
                icon="plus"
                onPress={() => this.sumar(this.state.valor1, this.state.valor2)}
              ></IconButton>
            </Surface>

            <View style={{ width: 20 }} />
            <Surface style={styles.surface}>
              <IconButton
                icon="minus"
                onPress={() =>
                  this.restar(this.state.valor1, this.state.valor2)
                }
              ></IconButton>
            </Surface>

            <View style={{ width: 20 }} />
            <Surface style={styles.surface}>
              <IconButton
                icon="percent"
                onPress={() =>
                  this.dividir(this.state.valor1, this.state.valor2)
                }
              ></IconButton>
            </Surface>

            <View style={{ width: 20 }} />
            <Surface style={styles.surface}>
              <IconButton
                icon="multiplication"
                onPress={() =>
                  this.multiplicar(this.state.valor1, this.state.valor2)
                }
              ></IconButton>
            </Surface>
          </View>
          <View style={{ width: 50, height: 50 }} />

          <Headline>Resultado: {this.state.resultado}</Headline>
        </View>
      </PaperProvider>
    );
  }

  setValor1 = (text) => {
    this.setState({ valor1: text });
  };

  setValor2 = (text) => {
    this.setState({ valor2: text });
  };

  sumar(a, b) {
    var a1 = parseInt(a);
    var b1 = parseInt(b);

    this.setState({ resultado: a1 + b1 });
  }
  restar(a, b) {    
    this.setState({ resultado: a - b });
  }
  dividir(a, b) {
    if (a == 0 || b == 0) {
      alert("No se puede dividir por cero");
    }
    this.setState({ resultado: a / b });
  }
  multiplicar(a, b) {
    this.setState({ resultado: a * b });
  }
}

const styles = StyleSheet.create({
  container: {
    //   flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    margin: "1em",
  },
  textInput: {
    margin: "1em",
  },
  surface: {
    marginTop: 10,
    padding: 8,
    height: 50,
    width: 50,
    alignItems: "center",
    justifyContent: "center",
    elevation: 4,
    borderRadius: 16,
  },
});
